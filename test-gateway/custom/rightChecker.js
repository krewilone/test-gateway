const axios = require("axios");

module.exports = {
    name: 'custom',
    schema: {
        $id: 'http://express-gateway.io/schemas/policies/right-check.json'
    },
    policy: () => {
        return (req, res, next) => {
            const [_, application, ...rest] = req.url.split("/");
            if(application === "service1") {
                axios.get("http://localhost:3004/app")
                    .then(data => data.data)
                    .then(data => {
                        if (!data.enabled) {
                            res.status(403);
                            res.json({reason: "You dont't have right"});
                            return;
                        }
                        console.log("forwarding to application", application);
                        req.url = `/${rest.join("/")}`;
                        return next();
                    });
                return;
            }
            console.log("forwarding to application", application);
            req.url = `/${rest.join("/")}`;
            return next();
        };
    }
};