module.exports = {
    version: '1.2.0',
    init: function (pluginContext) {
        let policy = require('./rightChecker');
        pluginContext.registerPolicy(policy)
    }
}