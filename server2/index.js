const express = require('express');
const axios = require("axios");
const cors = require("cors");
var bodyParser = require('body-parser');
const app = express();
const port = 4000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.post('/', (req, res) => {
    axios.post("http://localhost:3004/app", {
        enabled: req.body.enable
    }).then(() => {
        res.json({enabled: enabled})
    })
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));