const express = require('express');
const app = express();
const cors = require('cors');
const port = 3000;

app.use(cors());
app.get('/', (req, res) => res.json({response: 'Hello from service 1'}));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));